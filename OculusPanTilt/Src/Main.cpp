#include "App.hpp"
#include <bcm_host.h>
#include <string>
#include <iostream>

using namespace std;

int main(int argc, char **argv)
{
	try
	{
		bcm_host_init();
		(new App(0, 0, 1280, 800))->run();
		return 0;
	}
	catch (string & err)
	{
		cerr << "Caught exception" << endl << endl;
		cerr << err << endl << endl;
		return -1;
	}
}
