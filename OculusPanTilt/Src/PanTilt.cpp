/*
 * OpenHMD - Free and Open Source API and drivers for immersive technology.
 * Copyright (C) 2013 Fredrik Hultin.
 * Copyright (C) 2013 Jakob Bornecrantz.
 * Modified heavily by Jodie Wetherall <wj88@gre.ac.uk>
 * Distributed under the Boost 1.0 licence, see LICENSE for full text.
 */

#define GLM_FORCE_RADIANS

#include "PanTilt.hpp"

#include <string>
#include <iostream>

#include <openhmd.h>
#include <stdio.h>
#include <math.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <glm/gtc/constants.hpp>
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>
#include <glm/gtc/quaternion.hpp>
//#include <glm/glm.hpp>
//#include <glm/gtx/constants.hpp>
//#include <glm/gtx/quaternion.hpp>

using namespace std;

int pantilt = -1;
ohmd_context* ctx;
ohmd_device* hmd;

// gets float values from the device and prints them
void print_infof(ohmd_device* hmd, const char* name, int len, ohmd_float_value val)
{
        float f[len];
        ohmd_device_getf(hmd, val, f);
        printf("%-20s", name);
        for(int i = 0; i < len; i++)
                printf("%f ", f[i]);
        printf("\n");
}

void PanTilt::process_euler()
{
        // Extract the quat values from the oculus rift
        float quatArray[4];
        ohmd_device_getf(hmd, OHMD_ROTATION_QUAT, quatArray);

        // Note the ordering (w is last)
        float x = quatArray[0];
        float y = quatArray[1];
        float z = quatArray[2];
        float w = quatArray[3];

        // Get euler values from the quats
        glm::quat quatRot(w, x, y, z);
        glm::vec3 r = glm::eulerAngles(quatRot);

        // Take the pant and tilt values from the eulers
        // (x = pitch (tilt), y = yaw (pan), z = roll (unused))
        float tilt = r.x;
        float pan = r.y;

/*
The servo values have been manually derivied through trial and error.

Notes:

                        MaestroControlCentre    Bash Script
                        (quarter second)        (seconds - x4)

Tilt (0)

Horizontal              940                     3760
Vertical                1750                    7000
Max2400                 2400                    9600

Pan (1)

90 degree right         660                     2640
straight                1530                    6120
90 degree left          2450                    9800

*/

        // Translate the values into seconds of rotation
        int tiltI = 7000 - (int)(tilt / glm::pi<double>() * 6480); // (6480 = 180 degrees in seconds)
        int panI = 6120 - (int)(pan / -glm::pi<double>() * 7160);  // (7160 = 180 degrees in seconds)

        // Limit the rotations based on the pan/tilt hardware range
        if(tiltI < 3760) tiltI = 3760;
        if(tiltI > 9600) tiltI = 9600;
        if(panI < 2540) panI = 2540;
        if(panI > 9800) panI = 9800;

        // Output the values for tilt and pan to the servo controller
        char v[4];

        v[0] = 0x84;
        v[1] = 0;
        v[2] = tiltI & 0x7F;
        v[3] = (tiltI >> 7) & 0x7F;
        write(pantilt, v, 4);

        v[0] = 0x84;
        v[1] = 1;
        v[2] = panI & 0x7F;
        v[3] = (panI >> 7) & 0x7F;
        write(pantilt, v, 4);

        // Done.
}


PanTilt::PanTilt()
{
        // Open up the device to control the pan/tilt server controller
        pantilt = open("/dev/ttyACM0", O_RDWR);
	if(pantilt == -1)
	{
		cerr << "Failed to open the pan/tilt device at /dev/ttyACM0." << endl;
		throw new string("Failed to open the pan/tilt device at /dev/ttyACM0.");
	}

        // Create our ohmd context for accessing the oculus
        ctx = ohmd_ctx_create();

        // Probe for devices
        int num_devices = ohmd_ctx_probe(ctx);
        if(num_devices < 0)
	{
		cerr << "Failed to probe HMD devices: " << ohmd_ctx_get_error(ctx) << endl;
                throw new string("Failed to probe HMD devices.");
	}

        // Print the number of HMD devices.
        printf("Number of HMD devices: %d\n\n", num_devices);

        // Print the HMD device information
        for(int i = 0; i < num_devices; i++){
                printf("HMD device %d\n", i);
                printf("\tvendor:  %s\n", ohmd_list_gets(ctx, i, OHMD_VENDOR));
                printf("\tproduct: %s\n", ohmd_list_gets(ctx, i, OHMD_PRODUCT));
                printf("\tpath:    %s\n\n", ohmd_list_gets(ctx, i, OHMD_PATH));
        }

        // Open default device (0)
        hmd = ohmd_list_open_device(ctx, 0);

        // If this fails, exit.
        if(!hmd)
	{
		cerr << "Failed to open device: " << ohmd_ctx_get_error(ctx) << endl;
		throw new string("Failed to open device.");
	}

        // Print hardware information for the opened device
        int ivals[2];
        ohmd_device_geti(hmd, OHMD_SCREEN_HORIZONTAL_RESOLUTION, ivals);
        ohmd_device_geti(hmd, OHMD_SCREEN_VERTICAL_RESOLUTION, ivals + 1);
        printf("HMD Device info\n");
        printf("\tResolution:         %i x %i\n", ivals[0], ivals[1]);
        print_infof(hmd, "\thsize:",            1, OHMD_SCREEN_HORIZONTAL_SIZE);
        print_infof(hmd, "\tvsize:",            1, OHMD_SCREEN_VERTICAL_SIZE);
        print_infof(hmd, "\tlens separation:",  1, OHMD_LENS_HORIZONTAL_SEPARATION);
        print_infof(hmd, "\tlens vcenter:",     1, OHMD_LENS_VERTICAL_POSITION);
        print_infof(hmd, "\tleft eye fov:",     1, OHMD_LEFT_EYE_FOV);
        print_infof(hmd, "\tright eye fov:",    1, OHMD_RIGHT_EYE_FOV);
        print_infof(hmd, "\tleft eye aspect:",  1, OHMD_LEFT_EYE_ASPECT_RATIO);
        print_infof(hmd, "\tright eye aspect:", 1, OHMD_RIGHT_EYE_ASPECT_RATIO);
        print_infof(hmd, "\tdistortion k:",     6, OHMD_DISTORTION_K);

        printf("\n");

        // reset rotation and position
        float zero[] = {0, 0, 0, 1};
        ohmd_device_setf(hmd, OHMD_ROTATION_QUAT, zero);
        ohmd_device_setf(hmd, OHMD_POSITION_VECTOR, zero);
}

PanTilt::~PanTilt()
{
        ohmd_ctx_destroy(ctx);
        close(pantilt);
}

void PanTilt::process()
{
        ohmd_ctx_update(ctx);
        process_euler();
}
