#include "EGL.hpp"
#include <bcm_host.h>

EGLNativeWindowType EGL::createNativeWindow(int width, int height, int x, int y)
{
	uint32_t screen_width = 1280, screen_height = 800;
	
	VC_RECT_T dst_rect;
	dst_rect.x = 0;
	dst_rect.y = 0;
	dst_rect.width = screen_width;
	dst_rect.height = screen_height;
	
	VC_RECT_T src_rect;
	src_rect.x = 0;
	src_rect.y = 0;
	src_rect.width = screen_width << 16;
	src_rect.height = screen_height << 16;

	DISPMANX_DISPLAY_HANDLE_T dispman_display = vc_dispmanx_display_open( 0 );

	DISPMANX_UPDATE_HANDLE_T dispman_update = vc_dispmanx_update_start( 0 );
	DISPMANX_ELEMENT_HANDLE_T dispman_element = vc_dispmanx_element_add(
		dispman_update, dispman_display,
		0, &dst_rect, 0, &src_rect, DISPMANX_PROTECTION_NONE, 0, 0, DISPMANX_NO_ROTATE);

	static EGL_DISPMANX_WINDOW_T nativewindow;
	nativewindow.element = dispman_element;
	nativewindow.width = screen_width;
	nativewindow.height = screen_height;
	
	vc_dispmanx_update_submit_sync( dispman_update );
	
	return &nativewindow;
}
