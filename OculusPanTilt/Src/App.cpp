#include "App.hpp"
#include "PanTilt.hpp"

#include <time.h>
#include <iostream>

using namespace std;

void App::run()
{
	init();
	unsigned int frames = 0;
	int i = 0;
	time_t t1 = time(0);

	while (processEvents())
	{
		pantilt.process();
		update();
		display();
		egl.swap();

		frames++;
		time_t t2 = time(0);
		if (t2 - t1 >= 2.0f)
		{
			t1 = t2;
			cerr << frames << " frames rendered in 2 second -> FPS=" << (float)frames/ 2.0f << endl;
			frames = 0;
		}
	}
}

bool App::keys(unsigned char key, int x, int y)
{
	switch (key)
	{
		case 27:
		case 'q':
			return false;
			break;
	}
	return true;
}

void App::resize(int width, int height)
{
	this->width = width;
	this->height = height;
}

bool App::processEvents()
{
	return true;
}

void App::init()
{
	glDisable(GL_DEPTH_TEST);
	glClearColor(0.5, 0.5, 0.0, 1.0);

	glGenTextures(1, &imageTexture);
	glBindTexture(GL_TEXTURE_2D, imageTexture);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glViewport ( 0, 0, 1280, 800);
	program.use();
	textureAttribute = program.getLocation(ATTRIBUTE, "in_texture");
	vertexAttribute = program.getLocation(ATTRIBUTE, "in_position");
}

void App::update()
{
	bindNextFrame(imageTexture);
	if (program.link())
		program.use();
}

void App::display()
{
	static GLfloat vVertices[] = {
		-1, -1, 0,
		1, -1, 0,
		1, 1, 0,
		-1, 1, 0
	};
	static GLfloat vTex[] = {
		0, 0,
		1, 0,
		1, 1,
		0, 1,
	};

	glBindTexture(GL_TEXTURE_2D, imageTexture);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glVertexAttribPointer(vertexAttribute, 3, GL_FLOAT, GL_FALSE, 0, vVertices);
	glEnableVertexAttribArray(vertexAttribute);
	glVertexAttribPointer(textureAttribute, 2, GL_FLOAT, GL_FALSE, 0, vTex);
	glEnableVertexAttribArray(textureAttribute);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}
