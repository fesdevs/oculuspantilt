#pragma once

#include <EGL/egl.h>

class EGL
{
public:
	EGLNativeWindowType  hWnd;
	const EGLDisplay  display;
	EGLContext  context;
	EGLSurface  surface;
	
	EGLNativeWindowType createNativeWindow(int w, int h, int x, int y);
	
	EGLConfig init(EGLint * attribList);
public:
	EGL();
	void createWindow(int w = 1280, int h = 800, int x = 0, int y = 0);
	void surfaceless();
	void swap();
	void makeCurrent();
};


