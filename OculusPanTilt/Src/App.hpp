#pragma once

#include "EGL.hpp"
#include "Program.hpp"
#include "PanTilt.hpp"

class App
{
	PanTilt pantilt;
	Program program;
	GLuint imageTexture;
	GLuint vertexAttribute;
	GLuint textureAttribute;
	EGL egl;
	float xoffset, yoffset;
	float aspect;
	float zoom;

protected:
	int x, y;
	int width, height;

	void bindNextFrame(GLuint texture);

public:
	App(int x = 0, int y = 0, int width = 1280, int height = 800) : x(x), y(y), width(width), height(height), program("test"), pantilt()
	{
		egl.createWindow(width, height, x, y);
	}
	virtual ~App() {}

	void run();
	virtual bool keys(unsigned char key, int x, int y);
	virtual void resize(int width, int height);
	virtual bool processEvents();
	virtual void init();
	virtual void update();
	virtual void display();
};
