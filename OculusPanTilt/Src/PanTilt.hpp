#pragma once

class PanTilt
{
public:
	PanTilt();
	~PanTilt();
	void process();

private:
	void process_euler();
};