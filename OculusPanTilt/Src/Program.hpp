#pragma once

#include <string>
#include <map>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

using namespace std;

enum VarType
{
	UNIFORM,
	ATTRIBUTE,
};

struct Var
{
	string name;
	GLint size;
	GLenum type;
	GLuint id;
	GLint location;
	VarType vartype;
	Var() {}
	Var(VarType varType, GLuint program, GLuint id);
};

class Shader
{
	friend class Program;
	const GLenum type;
	GLuint shader;
	string sourceFile;
	time_t compiled;

public:
	Shader(GLenum type, const string & sourceFile);
	virtual ~Shader();
	static string getLog(GLuint shader);
	void compile();
	bool isStale();
};

class Program
{
	Shader vs;
	Shader fs;
	GLuint program;
	map<string, Var> vars;

public:
	Program(const string & name);
	virtual ~Program();
	GLint getLocation(VarType type, const string & name);
	void use();
	bool link();
	static string getLog(GLuint program);
};
