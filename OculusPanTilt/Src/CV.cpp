#include "App.hpp"
#include <opencv2/opencv.hpp>
#include <GLES2/gl2.h>

using namespace cv;
using namespace std;

void App::bindNextFrame(GLuint texture)
{
	static VideoCapture videoCaptureL(0);
	static VideoCapture videoCaptureR(1);
	static bool init = false;
	if (!init)
	{
		videoCaptureL.set(CV_CAP_PROP_FRAME_WIDTH, 800);
		videoCaptureL.set(CV_CAP_PROP_FRAME_HEIGHT, 600);
		videoCaptureR.set(CV_CAP_PROP_FRAME_WIDTH, 800);
		videoCaptureR.set(CV_CAP_PROP_FRAME_HEIGHT, 600);
		init = true;
	}

	static Mat frame;
	static Mat target(600, 960, CV_8UC3); // This has to be height, width
	static Mat left = target(Rect(0, 0, 480, 600));
	static Mat right = target(Rect(480, 0, 480, 600));

	if (!videoCaptureL.grab())
        	cerr << "Failed grab" << endl;
	else
	{
		videoCaptureL.retrieve(frame);
		Mat frameRoe(frame, Rect(160, 0, 480, 600));
		frameRoe.copyTo(left);
	}

	if (!videoCaptureR.grab())
		cerr << "Failed grab" << endl;
	else
	{
		videoCaptureR.retrieve(frame);
		Mat frameRoe(frame, Rect(160, 0, 480, 600));
		frameRoe.copyTo(right);
	}

	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, target.cols, target.rows, 0, GL_RGB, GL_UNSIGNED_BYTE, target.data);
}
