#!/bin/bash

sudo apt-get update
sudo apt-get --assume-yes install cmake autoconf libtool libudev-dev libusb-1.0 libglm-dev libcv-dev libopencv-dev

git clone https://github.com/signal11/hidapi.git
cd hidapi
./bootstrap
./configure
make
sudo make install
cd ..

git clone https://github.com/OpenHMD/OpenHMD.git
cd OpenHMD
./autogen.sh
./configure
make
sudo make install
cd ..

cd OculusPanTilt
mkdir build
cd build
cmake ../Src
make
cd ../..

sudo systemctl enable $(realpath ./service/OculusPanTilt.service)
sudo systemctl start OculusPanTilt.service
